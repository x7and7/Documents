#+TITLE: Haider's Homework file
#+AUTHOR: Haider Mirza
#+DATE: [2021-09-05 Sun]
#+DESCRIPTION: This file contains my homework.
#+SETUPFILE: /home/haiderm/Documents/Settings/jake-latex-standard.setup
#  _   _ __  ___
# | | | |  \/  |
# | |_| | |\/| | My Gitlab: https://github.com/Ha1derMirza
# |  _  | |  | | My Matrix: @haider.mirza:matrix.org
# |_| |_|_|  |_|

* About this document:
This is a personal file for personal scheduling and notation of anything about school.
This Org document is written in org their documentation is the following:
[[https://orgmode.org/][Org-Mode]] and [[https://www.gnu.org/software/emacs/][Emacs]]
* Timetable
[[/home/haiderm/Pictures/Org-Images/timetable.png]]
* Ongoing Homework
** Retention Homework 4
SCHEDULED: <2021-11-01 Mon>
Do da work man. u know what to do. It on teams.
** Half term homework (DFM)
SCHEDULED: <2021-11-01 Mon>
    Your half term homework is 20 questions on mixed topics from year 8. I have set this to be 1-3 level so some may be hard but please try your best. You should also make sure you have marked all of your retention homeworks and topic tests and fill in your tracker (see above) as I will do a book check next half term.

Good luck, and have a restful break when it comes.
** CAT 1
SCHEDULED: <2021-11-03 Wed>
Good afternoon, CAT 1 for Mathematics will take place on Wednesday 03/11.
This assessment will be NON CALCULATOR and will test topics taught over your time here at Aston.
We advise that you use your notebook, the online textbook and your Dr Frost Maths account to prepare and practice.
Using DFM, click the courses button to access teaching videos, key skills questions, exam style topic tests organised by topics in the relevant year group schemes of work.
* Completed Homework
** DONE Doctor Frost Maths 3
CLOSED: [2021-10-20 Wed 22:32] SCHEDULED: <2021-10-20 Wed>
Complete DFM
*** REASONING
Havent actually completed it but whatever.
** DONE Retention Homework 3
CLOSED: [2021-10-18 Mon 18:37] SCHEDULED: <2021-10-18 Mon>
Homework is retention homework.
If you do not remember the mathematical language around parallel lines I suggest that you revise this first.
*** REASONING
Completed the Retention Homeowork. There isnt even maths today.
** DONE Topic Test Online 1
CLOSED: [2021-10-17 Sun 16:32] SCHEDULED: <2021-10-12 Tue>
The homework is to complete the attached topic test in the back of your notebooks with full working.
No need to post it online.
** DONE Doctor Frost Maths 2
CLOSED: [2021-10-03 Sun 12:06] SCHEDULED: <2021-10-01 Fri>
Set on DFM, 20 questions on percentages.
*** REASONING
Actually did this on the day it's due. Just forgot to remove it.
** DONE Retention Homework 2
CLOSED: [2021-09-28 Tue 22:38] SCHEDULED: <2021-09-29 Wed>
This is the second retention homework. Please complete it with full working in the back of your notebook and upload your solutions to the page in OneNote.
*** REASONING
Completed it a day before (like right now) and my neck is hurting now but it is ok.
** DONE Doctor Frost Maths
CLOSED: [2021-09-19 Sun 21:17] SCHEDULED: <2021-09-20 Mon>
20 Questions to complete.
*** REASONING
Done it in a hurry cuz nearly late idk it done now and that is what matters.
** DONE Retention homework 1
CLOSED: [2021-09-16 Thu 21:58] SCHEDULED: <2021-09-17 Fri>
Answer the File containing some maths questions.
*** REASONING
Eh, not to hard some money questions and stuff imma go upload it now and stuff.

** DONE DFM
CLOSED: [2021-10-17 Sun 19:56] SCHEDULED: <2021-10-15 Fri>
Complete the work on DFM
